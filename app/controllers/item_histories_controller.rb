class ItemHistoriesController < ApplicationController

  respond_to :html, :js
  before_action :set_item_history, only: [:demand_update, :show, :update]
  before_action :demand_update, only: [:show]
  before_action :modal_responder, only: [:show]

  include Math
  require 'descriptive_statistics'

  def show
  end

  def update
    @item_history.update_attributes(item_history_params)

    # Atualiza a demanda total do item
    actual_total_demand = view_context.get_total_demand(@item_history)
    @item_history.item.update_attribute(:demand, actual_total_demand)

    # Atualiza o estoque mínimo do item
    demand_standard_deviation = demand_array(@item_history).standard_deviation
    security_factor = 1.645
    lead_time = 2.0
    minimum_stock = security_factor * demand_standard_deviation * Math.sqrt(lead_time)

    @item_history.item.update_attribute(:minimum_stock, minimum_stock)
    
    respond_to do |format|
      format.html { redirect_to items_path, notice: 'O histórico do item foi atualizado!' }
    end
  end

  private

  def set_item_history
    @item_history = ItemHistory.find(params[:id])
  end

  # Esse método atualiza as demandas totais e média, mas 
  # somente quando as demandas anteriores forem diferentes
  def demand_update

    previous_total_demand = @item_history.total
    actual_total_demand = view_context.get_total_demand(@item_history)
    media_demand = view_context.get_media_demand(@item_history)

    unless previous_total_demand == actual_total_demand
      @item_history.update_attribute(:total, actual_total_demand)
      @item_history.update_attribute(:media, media_demand)
    end

  end

  def modal_responder
    respond_modal_with set_item_history
  end

  def item_history_params
    params.require(:item_history).permit(item_history_periods_attributes: [:id, :period, :demand, :_destroy])
  end

end