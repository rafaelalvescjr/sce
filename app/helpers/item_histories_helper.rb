module ItemHistoriesHelper

  include Math
  require 'descriptive_statistics'

  def demand_array(item_history)
    array = []

    item_history.item_history_periods.each do |ihp|
      array << ihp.demand
    end

    array
  end

  def get_total_demand(item_history)
    demand_array(item_history).sum
  end

  def get_media_demand(item_history)
    demand_array(item_history).mean
  end

  # def get_minimum_stock(item_history)
  #   demand_standard_deviation = demand_array(item_history).standard_deviation
  #   security_factor = 1.645
  #   lead_time = 2.0
  #   mininum_stock = security_factor * demand_standard_deviation * Math.sqrt(lead_time)
  # end

end