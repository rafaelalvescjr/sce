module ItemsHelper

  def generate_report

    items = Item.all.includes(:item_history)

    Prawn::Document.generate(Rails.root.join('tmp').to_s + '/relatorio_de_estoque.pdf') do

      y_position = cursor - 10

      bounding_box([0, y_position], width: 200, height: 75) do
       text "Necessidades Urgentes S.A.", leading: 6, style: :bold
       text "Sistema de Controle de Estoque"
      end

      bounding_box([340, y_position], width: 200, height: 75) do
       text "Relatório de Situação de Estoque", leading: 6, style: :bold, align: :right
       data = Time.now.strftime("em %d/%m/%Y às %H:%Mh")
       text data, align: :right
      end

      data = [["Código  ", "Descrição", "Unidades\nExistentes", "Demanda\nTotal", "Demanda\nMédia", "Estoque Mínimo", "Renovação?"]]
      
      items.each do |item|

        if item.units < item.minimum_stock
          renovacao = "Sim"
        else
          renovacao = "Não"
        end

        data += [[item.code.to_s, item.description, item.units.to_s, 
                  item.demand.to_s, item.item_history.media.to_s,
                  item.minimum_stock.to_s, renovacao]]
      end

      table(data, header: true, row_colors: ["F0F0F0", "FFFFFF"], cell_style: { size: 10 })
    end

    Rails.root.join('tmp').to_s + '/relatorio_de_estoque.pdf'

  end

end
