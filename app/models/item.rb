class Item < ActiveRecord::Base
  has_one :item_history, dependent: :destroy
end
