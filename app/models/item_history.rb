class ItemHistory < ActiveRecord::Base

  belongs_to :item
  has_many :item_history_periods, dependent: :destroy
  accepts_nested_attributes_for :item_history_periods, reject_if: :all_blank, allow_destroy: true

end
