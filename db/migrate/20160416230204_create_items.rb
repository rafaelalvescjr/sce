class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :code
      t.string :description
      t.integer :units
      t.integer :demand

      t.timestamps null: false
    end
  end
end
