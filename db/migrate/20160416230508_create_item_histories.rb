class CreateItemHistories < ActiveRecord::Migration
  def change
    create_table :item_histories do |t|
      t.references :item, index: true, foreign_key: true
      t.integer :media

      t.timestamps null: false
    end
  end
end
