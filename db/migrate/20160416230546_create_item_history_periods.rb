class CreateItemHistoryPeriods < ActiveRecord::Migration
  def change
    create_table :item_history_periods do |t|
      t.references :item_history, index: true, foreign_key: true
      t.integer :period
      t.integer :demand

      t.timestamps null: false
    end
  end
end
