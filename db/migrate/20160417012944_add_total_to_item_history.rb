class AddTotalToItemHistory < ActiveRecord::Migration
  def change
    add_column :item_histories, :total, :integer
  end
end
