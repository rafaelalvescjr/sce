# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160417140419) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "item_histories", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "media"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "total"
  end

  add_index "item_histories", ["item_id"], name: "index_item_histories_on_item_id", using: :btree

  create_table "item_history_periods", force: :cascade do |t|
    t.integer  "item_history_id"
    t.integer  "period"
    t.integer  "demand"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "item_history_periods", ["item_history_id"], name: "index_item_history_periods_on_item_history_id", using: :btree

  create_table "items", force: :cascade do |t|
    t.integer  "code"
    t.string   "description"
    t.integer  "units"
    t.integer  "demand"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "minimum_stock"
  end

  add_foreign_key "item_histories", "items"
  add_foreign_key "item_history_periods", "item_histories"
end
