Item.destroy_all

50.times do

  item = Item.create(description: Faker::Commerce.product_name,
                     code: rand(2000..3000), units: rand(500..2000),
                     minimum_stock: 0)
  
  item_history = ItemHistory.create(item_id: item.id)

  for i in 1..10 do
    ItemHistoryPeriod.create(period: i, 
                             demand: rand(2200..4000),
                             item_history_id: item_history.id)
  end

end